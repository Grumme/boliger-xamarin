﻿using System;
using System.Collections.Generic;
using Xamarin.Forms.Maps;

namespace Boliger {
	// Used as a custom map to show more information than a normal map.
	// Works by dependency injection from the platform specific solutions.
	public class ResidenceMap : Map {

		public List<ResidencePin> ResidencePins { get; set; }

		public event EventHandler InfoTapped;

		// Used from the platform specific solutions to notify the shared solution that info is tapped
		public virtual void OnInfoTapped ( EventArgs e, Residence residence ) {
			EventHandler handler = InfoTapped;
			if ( handler != null ) {
				handler ( residence, e );
			}
		}

		public ResidenceMap ( ) {
			ResidencePins = new List<ResidencePin> ( );
		}

	}
}
