﻿using System;
namespace Boliger
{
	public class Address
	{
		public string City { get; set; }
		public string Street { get; set; }
		public int Zip { get; set; }

		public Address()
		{
		}
	}
}
