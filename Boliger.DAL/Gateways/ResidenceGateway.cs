﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using Newtonsoft.Json;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;

namespace Boliger.DAL {
	public class ResidenceGateway : IGateway<Residence> {

		HttpClient client;

		public ResidenceGateway ( ) {
			client = new HttpClient ( );
			client.MaxResponseContentBufferSize = 256000;
		}

		public void Create ( Residence element ) {
			throw new NotImplementedException ( );
		}

		public void Delete ( int id ) {
			throw new NotImplementedException ( );
		}

		public ObservableCollection<Residence> Read ( ) {
			using ( var client = new HttpClient ( ) ) {
				var url = new Uri ( Utilities.RestUrl + "/api/Residences" );

				HttpResponseMessage response = client.GetAsync ( url ).Result;

				var data = response.Content.ReadAsStringAsync ( ).Result;

				var residences = JsonConvert.DeserializeObject<ObservableCollection<Residence>> ( data );

				return response.IsSuccessStatusCode ? residences : null;
			}
		}

		public ObservableCollection<Residence> Read ( int index, int amount ) {
			using ( var client = new HttpClient ( ) ) {
				var url = new Uri ( Utilities.RestUrl + $"/api/Residences?index={index}&amount={amount}" );

				HttpResponseMessage response = client.GetAsync ( url ).Result;

				var data = response.Content.ReadAsStringAsync ( ).Result;

				var residences = JsonConvert.DeserializeObject<ObservableCollection<Residence>> ( data );

				//await Task.Delay ( 1000 );

				return response.IsSuccessStatusCode ? residences : null;
			}
		}

		public async Task<ObservableCollection<Residence>> ReadAsync ( ) {
			using ( var client = new HttpClient ( ) ) {
				var url = new Uri ( Utilities.RestUrl + "/api/Residences" );

				HttpResponseMessage response = await client.GetAsync ( url );

				var data = await response.Content.ReadAsStringAsync ( );

				var residences = JsonConvert.DeserializeObject<ObservableCollection<Residence>> ( data );

				//await Task.Delay ( 1000 );

				return response.IsSuccessStatusCode ? residences : null;
			}
		}

		public async Task<ObservableCollection<Residence>> ReadAsync ( int index, int amount ) {
			using ( var client = new HttpClient ( ) ) {
				var url = new Uri ( Utilities.RestUrl + $"/api/Residences?index={index}&amount={amount}" );

				HttpResponseMessage response = await client.GetAsync ( url );

				var data = await response.Content.ReadAsStringAsync ( );

				var residences = JsonConvert.DeserializeObject<ObservableCollection<Residence>> ( data );

				//await Task.Delay ( 1000 );

				return response.IsSuccessStatusCode ? residences : null;
			}
		}

		public Residence Read ( int id ) {
			throw new NotImplementedException ( );
		}

		public void Update ( Residence element ) {
			throw new NotImplementedException ( );
		}
	}
}
