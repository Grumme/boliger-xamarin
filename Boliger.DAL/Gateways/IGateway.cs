﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;

namespace Boliger.DAL {
	public interface IGateway<T> {
		void Create ( T element );

		T Read ( int id );

		ObservableCollection<T> Read ( );

		ObservableCollection<T> Read ( int index, int amount );

		Task<ObservableCollection<T>> ReadAsync ( );

		Task<ObservableCollection<T>> ReadAsync ( int index, int amount );

		void Update ( T element );

		void Delete ( int id );
	}
}
