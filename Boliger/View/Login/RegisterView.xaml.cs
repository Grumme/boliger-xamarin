using System;
using System.Collections.Generic;
using System.Diagnostics;
using Boliger.DAL;
using Xamarin.Forms;
using System.Linq;
using System.ComponentModel;

namespace Boliger {
	public partial class RegisterView : ContentPage {

		// isLoading properties is used when data is being loaded in the application
		// Makes a spinner visible when true
		private bool isLoading;
		public bool IsLoading {
			get {
				return this.isLoading;
			}

			set {
				this.isLoading = value;
				activityFrame.IsVisible = value;
				activityContent.IsVisible = value;
				activity.IsRunning = value;
			}
		}


		public RegisterView ( ) {
			InitializeComponent ( );

			// Set IsLoading to false as the view is just being shown and no login event has happened yet
			IsLoading = false;

			btnRegister.BackgroundColor = Utilities.ButtonColor;
			btnRegister.TextColor = Utilities.ButtonTextColor;
			btnRegister.HeightRequest = Utilities.ButtonHeight;

			Title = "Indtast oplysninger";

			btnRegister.Clicked += BtnRegister_Clicked;
		}

		// Returns a boolean based on if all inputs contains text
		bool isTxtFieldsEmpty ( ) {

			if ( txtMail.Text.IsNullOrEmpty ( ) || txtPhone.Text.IsNullOrEmpty ( ) || txtFirstName.Text.IsNullOrEmpty ( ) || txtLastName.Text.IsNullOrEmpty ( ) || txtPhone.Text.IsNullOrEmpty ( ) || txtZip.Text.IsNullOrEmpty ( ) || txtAddress.Text.IsNullOrEmpty ( ) || txtCity.Text.IsNullOrEmpty ( ) || txtPassword.Text.IsNullOrEmpty ( ) || txtConfirmPassword.Text.IsNullOrEmpty ( ) ) {
				return true;
			}

			return false;
		}

		// Event fired when the register button is clicked
		async void BtnRegister_Clicked ( object sender, EventArgs e ) {
			if ( !isTxtFieldsEmpty ( ) ) {

				// set IsLoading to true because we are starting to load data
				IsLoading = true;

				// Grab information from inputs to create a User we can send to backend
				var user = new User {
					Email = txtMail.Text,
					FirstName = txtFirstName.Text,
					LastName = txtLastName.Text,
					PhoneNumber = txtPhone.Text,
					Address = new Address {
						Street = txtAddress.Text,
						City = txtCity.Text,
						Zip = int.Parse ( txtZip.Text )
					}
				};

				// Send register request to backend containing the created user and password
				var success = await new Facade ( ).GetUserGateway ( ).RegisterAsync ( user, txtPassword.Text, txtConfirmPassword.Text );

				if ( success ) {
					await Navigation.PopAsync ( true );
					IsLoading = false;
				} else {
					IsLoading = false;
					await DisplayAlert ( "Fejl", "Tjek venligst dine oplysninger igen", "OK" );
				}
			} else {
				await DisplayAlert ( "Fejl", "Alle felterne skal udfyldes", "OK" );
			}
		}
	}
}
