﻿using System;
using System.Collections.Generic;
using Boliger.DAL;
using Xamarin.Forms;

namespace Boliger {
	public partial class ChangePasswordView : ContentPage {

		// isLoading properties is used when data is being loaded in the application
		// Makes a spinner visible when true
		private bool isLoading;
		public bool IsLoading {
			get {
				return this.isLoading;
			}

			set {
				this.isLoading = value;
				activityFrame.IsVisible = value;
				activityContent.IsVisible = value;
				activity.IsRunning = value;
			}
		}

		public ChangePasswordView ( ) {
			InitializeComponent ( );

			// Set IsLoading to false as the view is just being shown and no login event has happened yet
			IsLoading = false;

			btnChangePassword.BackgroundColor = Utilities.ButtonColor;
			btnChangePassword.TextColor = Utilities.ButtonTextColor;
			btnChangePassword.HeightRequest = Utilities.ButtonHeight;

			Title = "Skift kodeord";

			btnChangePassword.Clicked += BtnChangePassword_Clicked;
		}

		// Event fired when the change password button is clicked
		async void BtnChangePassword_Clicked ( object sender, EventArgs e ) {
			IsLoading = true;

			// Send changepassword requests to the backend with the current, new and confirm password from the inputs
			var success = await new Facade ( ).GetUserGateway ( ).ChangePasswordAsync ( txtOldPassword.Text, txtNewPassword.Text, txtConfirmPassword.Text );

			if ( success ) {
				// Change password was a success and we navigate the user back
				await Navigation.PopAsync ( true );
				IsLoading = false;
			} else {
				// An error occured and we tell the user that one of his passwords didnt match
				IsLoading = false;
				await DisplayAlert ( "Fejl", "En af dine adgangskoder er forkerte", "OK" );
			}
		}
	}
}
