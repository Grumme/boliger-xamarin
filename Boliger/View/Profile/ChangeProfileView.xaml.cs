﻿using System;
using System.Collections.Generic;
using Boliger.DAL;
using Xamarin.Forms;

namespace Boliger {
	public partial class ChangeProfileView : ContentPage {

		private User _user;

		// isLoading properties is used when data is being loaded in the application
		// Makes a spinner visible when true
		private bool isLoading;
		public bool IsLoading {
			get {
				return this.isLoading;
			}

			set {
				this.isLoading = value;
				activityFrame.IsVisible = value;
				activityContent.IsVisible = value;
				activity.IsRunning = value;
			}
		}

		public ChangeProfileView ( User user ) {
			InitializeComponent ( );

			_user = user;

			// Set IsLoading to false as the view is just being shown and no login event has happened yet
			IsLoading = false;

			btnChangeProfile.BackgroundColor = Utilities.ButtonColor;
			btnChangeProfile.TextColor = Utilities.ButtonTextColor;
			btnChangeProfile.HeightRequest = Utilities.ButtonHeight;

			Title = "Ret profil";

			// Fill the inputs with text from the sent in User
			txtFirstName.Text = user.FirstName;
			txtLastName.Text = user.LastName;
			txtPhone.Text = user.PhoneNumber;
			txtZip.Text = user.Address.Zip.ToString ( );
			txtCity.Text = user.Address.City;
			txtAddress.Text = user.Address.Street;

			btnChangeProfile.Clicked += BtnChangeProfile_Clicked;
		}

		// Event fired when the change profile button is clicked
		async void BtnChangeProfile_Clicked ( object sender, EventArgs e ) {
			IsLoading = true;

			// Update the user with the new data from the inputs
			_user.FirstName = txtFirstName.Text;
			_user.LastName = txtLastName.Text;
			_user.PhoneNumber = txtPhone.Text;
			_user.Address.Zip = int.Parse ( txtZip.Text );
			_user.Address.Street = txtAddress.Text;
			_user.Address.City = txtCity.Text;

			// Fire the changeprofile request to the backend
			var success = await new Facade ( ).GetUserGateway ( ).ChangeProfileAsync ( _user );

			// Navigate back to profile view if succesful request otherwise tell user that an error happened
			if ( success ) {
				await Navigation.PopAsync ( true );
				IsLoading = false;
			} else {
				IsLoading = false;
				await DisplayAlert ( "Fejl", "Der skete en fejl", "OK" );
			}
		}
	}
}
