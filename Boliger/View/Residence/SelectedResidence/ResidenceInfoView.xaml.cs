﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace Boliger {
	public partial class ResidenceInfoView : ContentView {
		public ResidenceInfoView ( Residence residence ) {
			InitializeComponent ( );

			txtDescription.FontFamily = Utilities.MainFont;

			txtRentLabel.FontFamily = Utilities.MainFont;
			txtRent.FontFamily = Utilities.MainFont;

			txtSize.FontFamily = Utilities.MainFont;
			txtSizeLabel.FontFamily = Utilities.MainFont;

			txtRooms.FontFamily = Utilities.MainFont;
			txtRoomsLabel.FontFamily = Utilities.MainFont;

			txtDeposit.FontFamily = Utilities.MainFont;
			txtDepositLabel.FontFamily = Utilities.MainFont;

			txtAconto.FontFamily = Utilities.MainFont;
			txtAcontoLabel.FontFamily = Utilities.MainFont;

			txtPrepaidRent.FontFamily = Utilities.MainFont;
			txtPrepaidRentLabel.FontFamily = Utilities.MainFont;

			txtFurnished.FontFamily = Utilities.MainFont;
			txtFurnishedLabel.FontFamily = Utilities.MainFont;

			txtAnimalLabel.FontFamily = Utilities.MainFont;
			txtAnimal.FontFamily = Utilities.MainFont;

			txtEnergy.FontFamily = Utilities.MainFont;
			txtEnergyLabel.FontFamily = Utilities.MainFont;

			txtCreation.FontFamily = Utilities.MainFont;
			txtCreationLabel.FontFamily = Utilities.MainFont;


			txtTitle.Text = residence.Title;
			txtDescription.Text = residence.Description;


			txtRent.Text = residence.Rent.ToString ( );
			txtSize.Text = residence.Size.ToString ( );
			txtRooms.Text = residence.Rooms.ToString ( );
			txtDeposit.Text = residence.Deposit.ToString ( );
			txtAconto.Text = residence.Aconto.ToString ( );
			txtPrepaidRent.Text = residence.PrepaidRent.ToString ( );
			txtFurnished.Text = residence.Furnished ? "Ja" : "Nej";
			txtAnimal.Text = residence.Animal ? "Ja" : "Nej";
			txtEnergy.Text = residence.Energy;
			txtCreation.Text = residence.CreationDate.ToString ( "dd MMMM yyyy" );


		}
	}
}
