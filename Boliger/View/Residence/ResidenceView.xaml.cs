using System;
using System.Collections.Generic;
using Boliger.DAL;
using Xamarin.Forms;
using System.Diagnostics;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Linq;

namespace Boliger {
	public partial class ResidenceView : ContentPage {
		private ObservableCollection<Residence> _residences;
		private ListView _residenceList;

		public ResidenceView ( ) {
			InitializeComponent ( );

			//Hides navigation bar
			NavigationPage.SetHasNavigationBar ( this, true );
			Title = "Boliger";

			// Read the first number of residences
			_residences = new Facade ( ).GetResidenceGateway ( ).Read ( 0, Utilities.ResidencesPerLoad );

			_residenceList = new ResidenceListView ( ListViewCachingStrategy.RetainElement, _residences );
			// Set the lists itemssource as the read in residences
			_residenceList.ItemsSource = _residences;

			_residenceList.SeparatorVisibility = SeparatorVisibility.None;
			_residenceList.BackgroundColor = Color.Silver;

			// Fires an event when a residence in the list is clicked
			_residenceList.ItemSelected += SelectedResidence;

			// Enables pull to refresh on the list
			_residenceList.IsPullToRefreshEnabled = true;
			// The command to be called when a user pulls to refresh
			_residenceList.RefreshCommand = new Command ( LoadResidencesCommand );

			// Check if the newest visible item is the last in the list
			// If it is the case we will try to read in more residences (Infinite scrolling)
			_residenceList.ItemAppearing += async ( sender, e ) => {
				if ( e.Item == _residences.Last ( ) ) {
					var newlyLoadedResidences = await new Facade ( ).GetResidenceGateway ( ).ReadAsync ( _residences.Count ( ) + 1, Utilities.ResidencesPerLoad );
					foreach ( var residence in newlyLoadedResidences ) {
						// Add the newly loaded "page" of residences to the list
						_residences.Add ( residence );
					}
				}
			};

			Content = _residenceList;
		}

		protected override void OnDisappearing ( ) {
			base.OnDisappearing ( );

			// Set the selected item in the list to null when view is disappearing
			// So that the cell is not selected when we return
			_residenceList.SelectedItem = null;
		}

		// Navigates to the selectedResidenceView and sends in clicked on Residence
		public async void SelectedResidence ( object sender, SelectedItemChangedEventArgs e ) {
			if ( _residenceList.SelectedItem != null ) {
				var selectedIndex = _residences.IndexOf ( ( Boliger.Residence ) _residenceList.SelectedItem );
				var selectedResidence = _residences[ selectedIndex ];

				await Navigation.PushAsync ( new SelectedResidenceView ( selectedResidence ) );
			}
		}

		// Loads new residences created in the system
		private async void LoadResidencesCommand ( ) {
			var newResidences = await new Facade ( ).GetResidenceGateway ( ).ReadAsync ( );
			foreach ( var residence in newResidences ) {
				// Check if the id of the read residences are higher than the first residence in the list
				if ( residence.Id > _residences.First ( ).Id ) {
					// If it is the case we add the new residence as the first position in the list, as it is the newest residnce in the system
					_residences.Insert ( 0, residence );
				}
			}

			// Set IsRefreshing to false as we are done loading new data
			_residenceList.IsRefreshing = false;
		}
	}

	// An extension on the ListView to style our own list
	public class ResidenceListView : ListView {
		private ObservableCollection<Residence> _residences;

		public ResidenceListView ( ListViewCachingStrategy strategy, ObservableCollection<Residence> residences ) : base ( strategy ) {
			ItemTemplate = new DataTemplate ( typeof ( ResidenceCell ) );
			RowHeight = 125;
			this._residences = residences;
		}

		protected override void SetupContent ( Cell content, int index ) {
			base.SetupContent ( content, index );

			var currentViewCell = content as ResidenceCell;

			if ( currentViewCell != null ) {
				currentViewCell.setupCell ( _residences[ index ] );
			}
		}
	}

	// An extension on the ViewCell to style our own cell
	public class ResidenceCell : ViewCell {
		public ResidenceCell ( ) {

		}

		public void setupCell ( Residence residence ) {
			var masterLayout = new StackLayout ( );
			masterLayout.Padding = Utilities.MainPadding;


			var horizontalLayout = new StackLayout {
				Orientation = StackOrientation.Horizontal
			};

			horizontalLayout.BackgroundColor = Color.White;
			horizontalLayout.Padding = Utilities.MainPadding;

			var image = new Image ( );

			if ( residence.Images == null || residence.Images.Count < 1 ) {
				residence.Images = new List<ResidenceImage> ( );
				residence.Images.Add ( new ResidenceImage { Image = Utilities.PlaceHolderImg } );
			}

			image.Source = ImageSource.FromUri ( new Uri ( residence.Images[ 0 ].Image ) );

			image.Aspect = Aspect.AspectFill;
			image.WidthRequest = 175;
			image.HeightRequest = 100;

			//Label stacklayout in right side
			var verticalLayout = new StackLayout {
				Orientation = StackOrientation.Vertical

			};

			var lblAddress = new Label { FontFamily = Utilities.MainFont };
			var lblSize = new Label { FontFamily = Utilities.MainFont };
			var lblRooms = new Label { FontFamily = Utilities.MainFont };
			var lblRent = new Label { FontFamily = Utilities.MainFont };



			verticalLayout.Children.Add ( lblAddress );
			verticalLayout.Children.Add ( lblSize );
			verticalLayout.Children.Add ( lblRooms );
			verticalLayout.Children.Add ( lblRent );

			lblAddress.Text = residence.Address.Street;
			lblSize.Text = "Størrelse: " + residence.Size.ToString ( );
			lblRooms.Text = "Værelser: " + residence.Rooms.ToString ( );
			lblRent.Text = "Husleje: " + residence.Rent.ToString ( );


			horizontalLayout.Children.Add ( image );

			horizontalLayout.Children.Add ( verticalLayout );

			masterLayout.Children.Add ( horizontalLayout );

			View = masterLayout;
		}
	}
}