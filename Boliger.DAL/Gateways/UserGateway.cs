﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net.Http;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Threading.Tasks;

namespace Boliger.DAL {
	public class UserGateway : IUserGateway {

		HttpClient client;

		public UserGateway ( ) {
			client = new HttpClient ( );
			client.MaxResponseContentBufferSize = 256000;
		}

		public User Read ( string id ) {
			using ( var client = new HttpClient ( ) ) {
				var url = new Uri ( Utilities.RestUrl + $"/api/Users/{id}" );

				AddAuthorizationHeader ( client );

				HttpResponseMessage response = client.GetAsync ( url ).Result;

				var data = response.Content.ReadAsStringAsync ( ).Result;

				return response.IsSuccessStatusCode ? JsonConvert.DeserializeObject<User> ( data ) : null;
			}
		}

		public async Task<User> ReadAsync ( string id ) {
			using ( var client = new HttpClient ( ) ) {
				var url = new Uri ( Utilities.RestUrl + $"/api/Users/{id}" );

				AddAuthorizationHeader ( client );

				HttpResponseMessage response = await client.GetAsync ( url );

				var data = await response.Content.ReadAsStringAsync ( );

				//await Task.Delay ( 5000 );

				return response.IsSuccessStatusCode ? JsonConvert.DeserializeObject<User> ( data ) : null;
			}
		}

		public User GetLoggedInUser ( ) {
			using ( var client = new HttpClient ( ) ) {
				var url = new Uri ( Utilities.RestUrl + "/api/Account/GetUserLoggedIn" );

				AddAuthorizationHeader ( client );

				HttpResponseMessage response = client.GetAsync ( url ).Result;

				var data = response.Content.ReadAsStringAsync ( ).Result;

				return response.IsSuccessStatusCode ? JsonConvert.DeserializeObject<User> ( data ) : null;
			}
		}

		public async Task<bool> LoginAsync ( string username, string password ) {
			using ( var client = new HttpClient ( ) ) {
				var url = new Uri ( Utilities.RestUrl + "/Token" );

				var formContent = new FormUrlEncodedContent ( new[ ] {
					new KeyValuePair<string, string>("grant_type", "password"),
					new KeyValuePair<string, string>("username", username),
					new KeyValuePair<string, string>("password", password)
				} );

				var response = await client.PostAsync ( url, formContent );

				if ( response.IsSuccessStatusCode ) {
					var responseJson = response.Content.ReadAsStringAsync ( ).Result;
					var jObject = JObject.Parse ( responseJson );
					string token = jObject.GetValue ( "access_token" ).ToString ( );

					Boliger.Helpers.Settings.GeneralSettings = token;

					Debug.WriteLine ( token );
				}

				//await Task.Delay ( 5000 );

				return response.IsSuccessStatusCode;
			}

		}

		public void Logout ( ) {
			throw new NotImplementedException ( );
		}

		public async Task<bool> RegisterAsync ( User user, string password, string confirmPassword ) {

			var url = new Uri ( Utilities.RestUrl + "/api/Account/Register" );

			try {
				var userModel = new RegisterUserModel {
					Email = user.Email,
					Password = password,
					ConfirmPassword = confirmPassword,
					FirstName = user.FirstName,
					LastName = user.LastName,
					Address = user.Address,
					PhoneNumber = user.PhoneNumber
				};

				var json = JsonConvert.SerializeObject ( userModel );
				var content = new StringContent ( json, Encoding.UTF8, "application/json" );

				HttpResponseMessage response = await client.PostAsync ( url, content );

				//await Task.Delay ( 5000 );

				return response.IsSuccessStatusCode;
			} catch ( Exception e ) {
				Debug.WriteLine ( e.ToString ( ) );
				return false;
			}
		}

		public async Task<bool> ChangePasswordAsync ( string oldPassword, string newPassword, string confirmPassword ) {
			using ( var client = new HttpClient ( ) ) {
				var url = new Uri ( Utilities.RestUrl + "/api/Account/ChangePassword" );

				AddAuthorizationHeader ( client );

				var model = new ChangePasswordModel {
					OldPassword = oldPassword,
					NewPassword = newPassword,
					ConfirmPassword = confirmPassword
				};

				var json = JsonConvert.SerializeObject ( model );
				var content = new StringContent ( json, Encoding.UTF8, "application/json" );

				HttpResponseMessage response = await client.PostAsync ( url, content );

				var data = await response.Content.ReadAsStringAsync ( );

				return response.IsSuccessStatusCode;
			}
		}

		public async Task<bool> ChangeProfileAsync ( User user ) {
			using ( var client = new HttpClient ( ) ) {
				var url = new Uri ( Utilities.RestUrl + "/api/Users" );

				AddAuthorizationHeader ( client );

				var model = new ChangeProfileModel {
					Id = user.Id,
					FirstName = user.FirstName,
					LastName = user.LastName,
					PhoneNumber = user.PhoneNumber,
					Address = user.Address
				};

				var json = JsonConvert.SerializeObject ( model );
				var content = new StringContent ( json, Encoding.UTF8, "application/json" );

				HttpResponseMessage response = await client.PutAsync ( url, content );

				var data = await response.Content.ReadAsStringAsync ( );

				return response.IsSuccessStatusCode;
			}
		}

		private void AddAuthorizationHeader ( HttpClient client ) {
			if ( !Boliger.Helpers.Settings.GeneralSettings.IsNullOrEmpty ( ) ) {
				string token = Boliger.Helpers.Settings.GeneralSettings.ToString ( );
				client.DefaultRequestHeaders.Add ( "Authorization", "Bearer " + token );
			}
		}
	}

	public class RegisterUserModel {
		public String Email { get; set; }
		public String Password { get; set; }
		public String ConfirmPassword { get; set; }
		public String FirstName { get; set; }
		public String LastName { get; set; }
		public String PhoneNumber { get; set; }
		public Address Address { get; set; }
	}

	public class ChangePasswordModel {
		public String OldPassword { get; set; }
		public String NewPassword { get; set; }
		public String ConfirmPassword { get; set; }
	}

	public class ChangeProfileModel {
		public String Id { get; set; }
		public String FirstName { get; set; }
		public String LastName { get; set; }
		public String PhoneNumber { get; set; }
		public Address Address { get; set; }
	}
}
