﻿using Xamarin.Forms;

namespace Boliger {
	public partial class App : Application {
		public App ( ) {
			InitializeComponent ( );

			// Wrap LoginPage in a navigationpage so that we can navigate to registerpage
			var navigationPage = new NavigationPage ( new LoginView ( ) );

			navigationPage.BarBackgroundColor = Utilities.MainColor;
			navigationPage.BarTextColor = Utilities.ButtonTextColor;

			MainPage = navigationPage;
		}

		public static Page GetSelectedResidenceView ( Residence residence ) {
			return new SelectedResidenceView ( residence );
		}

		protected override void OnStart ( ) {
			// Handle when your app starts
		}

		protected override void OnSleep ( ) {
			// Handle when your app sleeps
		}

		protected override void OnResume ( ) {
			// Handle when your app resumes
		}
	}
}
