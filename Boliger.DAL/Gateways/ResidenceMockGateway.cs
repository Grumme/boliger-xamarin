using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Boliger.DAL {
	public class ResidenceMockGateway {
		ObservableCollection<Residence> _residences;

		public ResidenceMockGateway ( ) {
			_residences = new ObservableCollection<Residence> ( );


			var address = new Address {
				Street = "Vesterhavsgade 10",
				City = "Esbjerg",
				Zip = 6700
			};

			var images = new List<ResidenceImage> ( );

			//images.Add("et.png");

			images.Add ( new ResidenceImage { Image = "https://home.mindworking.eu/resources/shops/714/cases/714A02959/casemedia/images/f09c4298da5ad34260d25cafc45f841a/customsize.jpg?deviceId=jd83hsdf3&width=1920" } );
			images.Add ( new ResidenceImage { Image = "https://home.mindworking.eu/resources/shops/714/cases/714A02959/casemedia/images/f09c4298da5ad34260d25cafc45f841a/customsize.jpg?deviceId=jd83hsdf3&width=1920" } );
			images.Add ( new ResidenceImage { Image = "https://home.mindworking.eu/resources/shops/714/cases/714A02959/casemedia/images/f09c4298da5ad34260d25cafc45f841a/customsize.jpg?deviceId=jd83hsdf3&width=1920" } );
			images.Add ( new ResidenceImage { Image = "https://home.mindworking.eu/resources/shops/714/cases/714A02959/casemedia/images/f09c4298da5ad34260d25cafc45f841a/customsize.jpg?deviceId=jd83hsdf3&width=1920" } );
			images.Add ( new ResidenceImage { Image = "https://home.mindworking.eu/resources/shops/714/cases/714A02959/casemedia/images/f09c4298da5ad34260d25cafc45f841a/customsize.jpg?deviceId=jd83hsdf3&width=1920" } );



			var residence1 = new Residence {
				Size = 100,
				Rooms = 10,
				Rent = 3875,
				Address = address,
				Images = images
			};

			var residence2 = new Residence {
				Size = 375,
				Rooms = 2,
				Rent = 7800,
				Address = address,
				Images = images
			};

			var residence3 = new Residence {
				Size = 375,
				Rooms = 2,
				Rent = 7800,
				Address = address,
				Images = images
			};

			var residence4 = new Residence {
				Size = 375,
				Rooms = 2,
				Rent = 7800,
				Address = address,
				Images = images
			};

			var residence5 = new Residence {
				Size = 375,
				Rooms = 2,
				Rent = 7800,
				Address = address,
				Images = images
			};

			_residences.Add ( residence1 );
			_residences.Add ( residence2 );
			_residences.Add ( residence3 );
			_residences.Add ( residence4 );
			_residences.Add ( residence5 );

		}

		public void Create ( Residence element ) {
			throw new NotImplementedException ( );
		}

		public void Delete ( int id ) {
			throw new NotImplementedException ( );
		}

		public ObservableCollection<Residence> Read ( ) {
			return this._residences;
		}

		public Residence Read ( int id ) {
			throw new NotImplementedException ( );
		}

		public void Update ( Residence element ) {
			throw new NotImplementedException ( );
		}
	}
}
