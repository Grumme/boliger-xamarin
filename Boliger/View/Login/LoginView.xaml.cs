﻿using System;
using System.Diagnostics;
using Boliger.DAL;
using Xamarin.Forms;
using System.ComponentModel;
using System.Threading.Tasks;

namespace Boliger {
	public partial class LoginView : ContentPage {

		// isLoading properties is used when data is being loaded in the application
		// Makes a spinner visible when true
		private bool isLoading;
		public bool IsLoading {
			get {
				return this.isLoading;
			}
			set {
				this.isLoading = value;
				activityFrame.IsVisible = value;
				activityContent.IsVisible = value;
				activity.IsRunning = value;
			}
		}

		public LoginView ( ) {
			InitializeComponent ( );

			// Set IsLoading to false as the view is just being shown and no login event has happened yet
			IsLoading = false;

			Title = "Log Ind";

			btnSignIn.BackgroundColor = Utilities.ButtonColor;
			btnSignIn.TextColor = Utilities.ButtonTextColor;
			btnSignIn.HeightRequest = Utilities.ButtonHeight;

			btnRegister.BackgroundColor = Utilities.ButtonColor;
			btnRegister.TextColor = Utilities.ButtonTextColor;
			btnRegister.HeightRequest = Utilities.ButtonHeight;

			//Hides navigation bar
			NavigationPage.SetHasNavigationBar ( this, false );

			// Check if a token exists
			if ( !Boliger.Helpers.Settings.GeneralSettings.IsNullOrEmpty ( ) ) {
				// As a token already exists we try to get the user registered with that token
				var user = new Facade ( ).GetUserGateway ( ).GetLoggedInUser ( );

				// We check if we received a user
				if ( user != null ) {
					// The token was good and we received a user
					// So we proceed to the mainpage of the application
					Navigation.PushModalAsync ( new MainPage ( ), true );
				} else {
					// We did not receive a user and we clear the saved token
					// Token might have expired or the token is invalid
					Boliger.Helpers.Settings.GeneralSettings = "";
				}

			}
		}

		// Event fired when the login button is clicked
		async void LoginClicked ( object o, EventArgs e ) {
			// set IsLoading to true because we are starting to load data
			IsLoading = true;

			// Send token request to backend
			var success = await new Facade ( ).GetUserGateway ( ).LoginAsync ( txtMail.Text, txtPassword.Text );

			if ( success ) {
				// The login was succesful and we navigate the user to the mainpage
				await Navigation.PushModalAsync ( new MainPage ( ) );
				// Set IsLoading to false since we are done loading data
				IsLoading = false;
			} else {
				// Error in login so we alert the user and clear IsLoading
				IsLoading = false;
				await DisplayAlert ( "Fejl", "Dit brugernavn eller kodeord er forkert", "OK" );
			}

		}

		// Event fired when the register button is clicked
		void RegisterClicked ( object o, EventArgs e ) {
			Navigation.PushAsync ( new RegisterView ( ) );
		}
	}
}
