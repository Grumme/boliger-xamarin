﻿using System;
using System.Collections.Generic;
using Boliger.DAL;

namespace Boliger
{
	public class Residence
	{
		public int Id { get; set; }

		public string Aconto { get; set; }
		public bool Active { get; set; }
		public DateTime CreationDate { get; set; }
		public Address Address { get; set; }
		public bool Animal { get; set; }
		public int Deposit { get; set; }
		public string Description { get; set; }
		public string Energy { get; set; }
		public int Floor { get; set; }
		public bool Furnished { get; set; }
		public List<ResidenceImage> Images { get; set; }
		public int PrepaidRent { get; set; }
		public int Rent { get; set; }
		public string RentalPeriod { get; set; }
		public int Rooms { get; set; }
		public int Size { get; set; }
		public string TakeoverDate { get; set; }
		public string Title { get; set; }
		public string Type { get; set; }
		public string UserId { get; set; }
		public Position Position { get; set; }

		public Residence()
		{
		}
	}

	public class ResidenceImage {
		public int Id { get; set; }
		public String Image { get; set; }
	}
}
