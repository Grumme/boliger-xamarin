﻿using System;
using System.Collections.Generic;
using CoreGraphics;
using Foundation;
using Boliger;
using Boliger.iOS;
using MapKit;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Maps;
using Xamarin.Forms.Maps.iOS;
using Xamarin.Forms.Platform.iOS;
using System.Diagnostics;

[assembly: ExportRenderer ( typeof ( ResidenceMap ), typeof ( ResidenceMapRenderer ) )]
namespace Boliger.iOS {
	public class ResidenceMapRenderer : MapRenderer {
		UIView customPinView;
		List<ResidencePin> residencePins;

		ResidenceMap formsMap;

		protected override void OnElementChanged ( ElementChangedEventArgs<View> e ) {
			base.OnElementChanged ( e );

			if ( e.OldElement != null ) {
				var nativeMap = Control as MKMapView;
				nativeMap.GetViewForAnnotation = null;
				nativeMap.CalloutAccessoryControlTapped -= OnCalloutAccessoryControlTapped;
				nativeMap.DidSelectAnnotationView -= OnDidSelectAnnotationView;
				nativeMap.DidDeselectAnnotationView -= OnDidDeselectAnnotationView;
			}

			if ( e.NewElement != null ) {
				formsMap = ( ResidenceMap ) e.NewElement;
				var nativeMap = Control as MKMapView;
				residencePins = formsMap.ResidencePins;

				nativeMap.GetViewForAnnotation += GetViewForAnnotation;
				nativeMap.CalloutAccessoryControlTapped += OnCalloutAccessoryControlTapped;
				nativeMap.DidSelectAnnotationView += OnDidSelectAnnotationView;
				nativeMap.DidDeselectAnnotationView += OnDidDeselectAnnotationView;
			}
		}

		MKAnnotationView GetViewForAnnotation ( MKMapView mapView, IMKAnnotation annotation ) {
			MKAnnotationView annotationView = null;

			if ( annotation is MKUserLocation )
				return null;

			var anno = annotation as MKPointAnnotation;
			if ( anno == null ) {
				return null;
			}

			var customPin = GetCustomPin ( anno );
			if ( customPin == null ) {
				throw new Exception ( "Custom pin not found" );
			}

			annotationView = mapView.DequeueReusableAnnotation ( customPin.Pin.Label );
			if ( annotationView == null ) {
				annotationView = new ResidenceMKAnnotationView ( annotation, customPin.Pin.Label );
				annotationView.Image = UIImage.FromFile ( "pin.png" );
				annotationView.CalloutOffset = new CGPoint ( 0, 0 );
				annotationView.RightCalloutAccessoryView = UIButton.FromType ( UIButtonType.DetailDisclosure );
				( ( ResidenceMKAnnotationView ) annotationView ).Residence = customPin.Residence;
			}
			annotationView.CanShowCallout = true;

			return annotationView;
		}

		void OnCalloutAccessoryControlTapped ( object sender, MKMapViewAccessoryTappedEventArgs e ) {
			var test = e.View as ResidenceMKAnnotationView;

			var residence = test.Residence;

			formsMap.OnInfoTapped ( e, residence );
		}

		void OnDidSelectAnnotationView ( object sender, MKAnnotationViewEventArgs e ) {
			var customView = e.View as ResidenceMKAnnotationView;
			customPinView = new UIView ( );

			customPinView.Frame = new CGRect ( 0, 0, 300, 200 );
			var image = new UIImageView ( new CGRect ( 0, 0, 300, 200 ) );
			image.Image = ImageFromUrl ( customView.Residence.Images[ 0 ].Image );
			image.ContentMode = UIViewContentMode.ScaleAspectFit;
			customPinView.AddSubview ( image );
			customPinView.Center = new CGPoint ( 0, -( e.View.Frame.Height + 145 ) );
			e.View.AddSubview ( customPinView );
		}

		void OnDidDeselectAnnotationView ( object sender, MKAnnotationViewEventArgs e ) {
			if ( !e.View.Selected ) {
				customPinView.RemoveFromSuperview ( );
				customPinView.Dispose ( );
				customPinView = null;
			}
		}

		ResidencePin GetCustomPin ( MKPointAnnotation annotation ) {
			var position = new Position ( annotation.Coordinate.Latitude, annotation.Coordinate.Longitude );

			foreach ( var pin in residencePins ) {
				if ( pin.Pin.Position == position ) {
					return pin;
				}
			}
			return null;
		}

		static UIImage ImageFromUrl ( string uri ) {
			using ( var url = new NSUrl ( uri ) )
			using ( var data = NSData.FromUrl ( url ) )
				return UIImage.LoadFromData ( data );
		}
	}
}
