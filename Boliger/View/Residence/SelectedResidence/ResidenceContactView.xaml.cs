﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;
using System.Diagnostics;

namespace Boliger {
	public partial class ResidenceContactView : ContentView {

		private User _user;

		public ResidenceContactView ( User user ) {
			InitializeComponent ( );

			this._user = user;
		}

		// Event fired when the phone button is clicked
		void PhoneClicked ( object o, EventArgs e ) {
			// Check if the residences creator contains a phonenumber
			if ( !_user.PhoneNumber.IsNullOrEmpty ( ) ) {
				// Opens the devices built in phone app and calls the number
				Device.OpenUri ( new Uri ( String.Format ( "tel:{0}", _user.PhoneNumber ) ) );
			}
		}

		// Event fired when the mail button is clicked
		void MailClicked ( object o, EventArgs e ) {
			// Check if the residences creator contains a mail
			if ( !_user.PhoneNumber.IsNullOrEmpty ( ) ) {
				// Opens the devices built in mail app and creates a new empty email with the correct recipient
				Device.OpenUri ( new Uri ( String.Format ( "mailto:{0}", _user.Email ) ) );
			}
		}
	}
}
