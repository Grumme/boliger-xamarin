﻿using System;
using System.Collections.Generic;

namespace Boliger
{
	public class User
	{
		public string Id { get; set; }

		public string Email { get; set; }

		public string FirstName { get; set; }

		public string LastName { get; set; }

		public string PhoneNumber { get; set; }

		public Address Address { get; set; }

		public List<Residence> Residences { get; set; }

		public User()
		{
		}
	}
}
