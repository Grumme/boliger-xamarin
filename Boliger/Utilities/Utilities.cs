﻿using System;
using Xamarin.Forms;

namespace Boliger {
	// Global utility class which hold static information about color schemes, fonts, restUrls and other information
	static public class Utilities {
		public static string MainFont = "HelveticaNeue-Light";

		public static Color MainColor = Color.FromHex ("#EE5D33");

		public static Color BackgroundColor = Color.Silver;

		public static Color ButtonColor = Color.FromHex ( "#B71C1C" );

		public static Color ButtonTextColor = Color.White;

		public static int ButtonHeight = 40;

		public static int MainPadding = 10;

		public static string RestUrl = "https://dev.grumsen-it.dk/";

		public static string PlaceHolderImg = "https://grumsenit.dk/placeholder.jpg";

		public static int ResidencesPerLoad = 7;
	}
}
