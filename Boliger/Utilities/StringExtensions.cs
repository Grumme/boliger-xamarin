﻿using System;
namespace Boliger {
	public static class StringExtensions {

		// Method extension on the string class to know if the string is null or whitespace.
		public static bool IsNullOrEmpty(this string s) {
			return s == null || s.Length == 0;
		}
	}
}
