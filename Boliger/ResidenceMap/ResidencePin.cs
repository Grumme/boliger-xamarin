﻿using System;
using Xamarin.Forms.Maps;

namespace Boliger {
	// Used by the ResidenceMap to make a pin hold a Residence.
	public class ResidencePin {
		public Pin Pin { get; set; }
		public Residence Residence { get; set; }
	}
}
