﻿using System;
using System.Threading.Tasks;
namespace Boliger.DAL {
	public interface IUserGateway {
		Task<bool> RegisterAsync ( User user, string password, string confirmPassword );

		Task<bool> LoginAsync ( string username, string password );

		Task<bool> ChangePasswordAsync ( string oldPassword, string newPassword, string confirmPassword );

		Task<bool> ChangeProfileAsync ( User user );

		void Logout ( );

		User GetLoggedInUser ( );

		User Read ( String id );

		Task<User> ReadAsync ( String id );

		//void Update(User user);
	}
}
