﻿using System;
using MapKit;

namespace Boliger.iOS {
	public class ResidenceMKAnnotationView : MKAnnotationView {
		public Residence Residence { get; set; }

		public ResidenceMKAnnotationView ( IMKAnnotation annotation, string id )
					: base ( annotation, id ) {
		}
	}
}
