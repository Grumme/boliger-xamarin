using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace Boliger {
	public partial class ImageCollection : ContentView {

		public ImageCollection ( Residence residence ) {
			InitializeComponent ( );

			txtAddress.FontFamily = Utilities.MainFont;
			txtCity.FontFamily = Utilities.MainFont;

			txtType.Text = residence.Type;
			txtAddress.Text = residence.Address.Street;
			txtCity.Text = string.Format ( "{0} {1}", residence.Address.Zip, residence.Address.City );

			// Check if the residence contains any images
			// If not a new list is created and a ResidenceImage is added with a placeholder image
			// Failsafe if backend does not catch that there is no image added
			if ( residence.Images == null || residence.Images.Count < 1 ) {
				residence.Images = new List<ResidenceImage> ( );
				residence.Images.Add ( new ResidenceImage { Image = Utilities.PlaceHolderImg } );
			}

			// Load the first image in the list and set it as the big image
			bigImage.Source = ImageSource.FromUri ( new Uri ( residence.Images[ 0 ].Image ) );

			// Create a tapGestureRecognizer to know when a picture is clicked
			var imageTap = new TapGestureRecognizer ( );
			// ImageTap_Tapped is called whenever a image is clicked
			imageTap.Tapped += ImageTap_Tapped;
			// Run trough all images and add them as images in the grid created in xml
			for ( int i = 0; i < residence.Images.Count; i++ ) {
				Image image = new Image { Source = ImageSource.FromUri ( new Uri ( residence.Images[ i ].Image ) ), Aspect = Aspect.Fill };
				grid.Children.Add ( image, i, 0 );
				image.GestureRecognizers.Add ( imageTap );
			}

		}

		// When event is fired it will set the bigImages source to the sent in image
		void ImageTap_Tapped ( Object sender, EventArgs e ) {
			var senderImage = ( Image ) sender;
			bigImage.Source = senderImage.Source;
		}
	}
}
