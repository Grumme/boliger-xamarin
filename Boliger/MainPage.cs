﻿using System;
using Xamarin.Forms;

namespace Boliger {
	public class MainPage : TabbedPage {
		public MainPage ( ) {
			Children.Add ( SetupResidenceNav ( ) );

			// There is not implemented a custom maprenderer in the Android solution
			// Which is why we only add the big map tabbed page on IOS
			if ( Device.OS == TargetPlatform.iOS ) {
				Children.Add ( SetupMapNav ( ) );
			}

			Children.Add ( SetupProfileNav ( ) );

			//Hides navigation bar
			NavigationPage.SetHasNavigationBar ( this, false );
		}

		private NavigationPage SetupResidenceNav ( ) {
			var residenceNavigation = new NavigationPage ( new ResidenceView ( ) );
			residenceNavigation.Title = "Boliger";
			residenceNavigation.Icon = "largeHouse.png";
			residenceNavigation.BackgroundColor = Utilities.MainColor;
			residenceNavigation.BarBackgroundColor = Utilities.MainColor;
			residenceNavigation.BarTextColor = Utilities.ButtonTextColor;


			return residenceNavigation;
		}

		private NavigationPage SetupMapNav ( ) {
			var profileNavigation = new NavigationPage ( new ResidenceLocationMapView ( ) );
			profileNavigation.Title = "Kort";
			profileNavigation.Icon = "map.png";
			profileNavigation.BackgroundColor = Utilities.MainColor;
			profileNavigation.BarBackgroundColor = Utilities.MainColor;
			profileNavigation.BarTextColor = Utilities.ButtonTextColor;

			return profileNavigation;
		}

		private NavigationPage SetupProfileNav ( ) {
			var profileNavigation = new NavigationPage ( new ProfileView ( ) );
			profileNavigation.Title = "Profil";
			profileNavigation.Icon = "user.png";
			profileNavigation.BackgroundColor = Utilities.MainColor;
			profileNavigation.BarBackgroundColor = Utilities.MainColor;
			profileNavigation.BarTextColor = Utilities.ButtonTextColor;

			return profileNavigation;
		}
	}
}