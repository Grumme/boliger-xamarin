﻿using System;
namespace Boliger.DAL
{
	public class Facade
	{

		private static IGateway<Residence> _residenceGateway;

		public IGateway<Residence> GetResidenceGateway()
		{
			if (_residenceGateway == null)
			{
				_residenceGateway = new ResidenceGateway();
			}

			return _residenceGateway;
		}

		public IUserGateway GetUserGateway()
		{
			return new UserGateway();
		}
	}
}
