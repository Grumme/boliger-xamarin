using System;
using System.Collections.Generic;
using System.Diagnostics;
using Xamarin.Forms;
using Boliger.DAL;

namespace Boliger {
	public partial class SelectedResidenceView : ContentPage {

		private User _user;

		public SelectedResidenceView ( Residence residence ) {
			InitializeComponent ( );

			_user = new User { PhoneNumber = "", Email = "" };
			LoadUserAsync ( residence.UserId );

			Title = residence.Address.Street;

			stack.BackgroundColor = Color.Silver;
			stack.Padding = 10;
			stack.Children.Add ( new ImageCollection ( residence ) );
			stack.Children.Add ( new ResidenceInfoView ( residence ) );
			stack.Children.Add ( new ResidenceContactView ( _user ) );
			stack.Children.Add ( new ResidenceMapView ( residence ) );
		}

		// Loads the residences creator in an async way, so that the application doesnt freeze
		private async void LoadUserAsync ( string id ) {
			var user = await new Facade ( ).GetUserGateway ( ).ReadAsync ( id );

			_user.Email = user.Email;
			_user.PhoneNumber = user.PhoneNumber;
			_user.Id = user.Id;
			_user.Address = user.Address;
			_user.FirstName = user.FirstName;
			_user.LastName = user.LastName;
			_user.Residences = user.Residences;
		}
	}
}
