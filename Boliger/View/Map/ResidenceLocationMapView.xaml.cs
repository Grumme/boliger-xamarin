﻿using System;
using System.Collections.Generic;
using System.Linq;
using Plugin.Geolocator;
using Xamarin.Forms;
using Xamarin.Forms.Maps;
using Boliger.DAL;
using System.Collections.ObjectModel;
using System.Diagnostics;

namespace Boliger {
	// Big map for a tabbed page
	public partial class ResidenceLocationMapView : ContentPage {

		ResidenceMap _map;

		public ResidenceLocationMapView ( ) {
			InitializeComponent ( );
			Title = "Kort";

			// Create a custom ResidenceMap
			_map = new ResidenceMap {
				IsShowingUser = true,
				VerticalOptions = LayoutOptions.FillAndExpand,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				MapType = MapType.Street
			};

			_map.InfoTapped += _map_InfoTapped;

			stack.Children.Add ( _map );

			// Read the residences to show them on the map 
			var residences = new Facade ( ).GetResidenceGateway ( ).Read ( );

			// Use Geolocator package to geolocate positions from adresses
			GetLocations ( residences );
		}

		void _map_InfoTapped ( object sender, EventArgs e ) {
			var residence = sender as Residence;

			Navigation.PushAsync ( new SelectedResidenceView ( residence ), true );
		}

		// Creates a list of pins and adds them to the map with the geolocated adresses
		private async void GetLocations ( ObservableCollection<Residence> residences ) {
			var geoCoder = new Geocoder ( );

			foreach ( var residence in residences ) {
				// Format the residences address into a format easily accepted from the geolocator
				var address = string.Format ( "{0}, {1}", residence.Address.Street, residence.Address.City );

				// Receiving a potential position
				var positions = ( await geoCoder.GetPositionsForAddressAsync ( address ) ).ToList ( );
				// Check if we did receive a position
				if ( positions.Any ( ) ) {
					// Grab the first position
					var position = positions[ 0 ];

					// Create a pin with the received position
					var pin = new ResidencePin {
						Pin = new Pin {
							Type = PinType.Place,
							Position = position,
							Label = residence.Address.Street
						},
						Residence = residence
					};

					_map.ResidencePins.Add ( pin );
					_map.Pins.Add ( pin.Pin );
				}
			}

			// Get the users current position
			var locator = CrossGeolocator.Current;
			locator.DesiredAccuracy = 50;
			var devicePosition = await locator.GetPositionAsync ( 2000 );

			// Center the map on the users current position
			var mapDevicePosition = new Xamarin.Forms.Maps.Position ( devicePosition.Latitude, devicePosition.Longitude );
			_map.MoveToRegion ( MapSpan.FromCenterAndRadius ( mapDevicePosition, Distance.FromKilometers ( 2.0 ) ) );
		}
	}
}
