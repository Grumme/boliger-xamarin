﻿using System;
using System.Collections.Generic;
using System.Linq;
using Xamarin.Forms;
using Xamarin.Forms.Maps;
using Newtonsoft.Json.Serialization;

namespace Boliger {
	// Displays a small map for a single recidence
	public partial class ResidenceMapView : ContentView {
		Map map;

		public ResidenceMapView ( Residence residence ) {
			InitializeComponent ( );

			map = new Map {
				IsShowingUser = true,
				HeightRequest = 150,
				VerticalOptions = LayoutOptions.FillAndExpand,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				MapType = MapType.Street
			};

			// Get the position of the recidence and convert it to a Forms.Maps Position object
			var residencePos = new Position ( residence.Position.Latitude, residence.Position.Longitude );
			// Move the map to the residences position
			map.MoveToRegion ( MapSpan.FromCenterAndRadius ( residencePos, Distance.FromKilometers ( 0.3 ) ) );
			// Create a pin for the residence at the correct position
			var pin = new Pin {
				Type = PinType.Place,
				Position = residencePos,
				Label = residence.Address.Street
			};
			map.Pins.Add ( pin );

			stack.Children.Add ( map );
		}
	}
}