﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using Boliger.DAL;
using Xamarin.Forms;

namespace Boliger {
	public partial class ProfileView : ContentPage {

		IUserGateway _userGateway;
		User _user;

		public ProfileView ( ) {
			InitializeComponent ( );

			Title = "Profil";

			stack.BackgroundColor = Color.FromRgba ( 0, 0, 0, 0.2 );

			lblNameLabel.FontFamily = Utilities.MainFont;
			lblName.FontFamily = Utilities.MainFont;

			lblCity.FontFamily = Utilities.MainFont;
			lblCityLabel.FontFamily = Utilities.MainFont;

			lblEmail.FontFamily = Utilities.MainFont;
			lblEmailLabel.FontFamily = Utilities.MainFont;

			lblPhone.FontFamily = Utilities.MainFont;
			lblPhoneLabel.FontFamily = Utilities.MainFont;

			lblAddress.FontFamily = Utilities.MainFont;
			lblAddressLabel.FontFamily = Utilities.MainFont;

			btnChangePassword.BackgroundColor = Utilities.ButtonColor;
			btnChangePassword.TextColor = Utilities.ButtonTextColor;
			btnChangePassword.HeightRequest = Utilities.ButtonHeight;

			btnChangeProfile.BackgroundColor = Utilities.ButtonColor;
			btnChangeProfile.TextColor = Utilities.ButtonTextColor;
			btnChangeProfile.HeightRequest = Utilities.ButtonHeight;

			tbLogOut.Clicked += TbLogOut_Clicked;
			btnChangePassword.Clicked += BtnChangePassword_Clicked;
			btnChangeProfile.Clicked += BtnChangeProfile_Clicked;
		}

		protected override void OnAppearing ( ) {
			base.OnAppearing ( );

			_userGateway = new Facade ( ).GetUserGateway ( );

			_user = _userGateway.GetLoggedInUser ( );

			lblName.Text = string.Format ( "{0} {1}", _user.FirstName, _user.LastName );
			lblEmail.Text = string.Format ( "{0}", _user.Email );
			lblPhone.Text = string.Format ( "{0}", _user.PhoneNumber );

			lblAddress.Text = string.Format ( "{0}", _user.Address.Street );
			lblCity.Text = string.Format ( "{0} {1}", _user.Address.Zip, _user.Address.City );
		}

		// Log out of the application when logout button is clicked
		void TbLogOut_Clicked ( object sender, EventArgs e ) {
			// Remove the saved token
			Boliger.Helpers.Settings.GeneralSettings = "";
			// Pop modal async so we return to the login page
			Navigation.PopModalAsync ( true );
		}

		// Event fired when change password button is clicked
		// Navigates to the changepassword view
		void BtnChangePassword_Clicked ( object sender, EventArgs e ) {
			Navigation.PushAsync ( new ChangePasswordView ( ), true );
		}

		// Event fired when change profile button is clicked
		// Navigates to the changeprofile view
		void BtnChangeProfile_Clicked ( object sender, EventArgs e ) {
			Navigation.PushAsync ( new ChangeProfileView ( _user ), true );
		}
	}
}
